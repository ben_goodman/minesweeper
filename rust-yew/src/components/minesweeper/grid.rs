use yew::prelude::*;

use super::cell::*;

#[derive(Debug, Properties, PartialEq, Clone)]
pub struct CellGroupProps {
    #[prop_or_default]
    pub rows: u32,
    #[prop_or_default]
    pub columns: u32,
    #[prop_or_default]
    pub mines: u32,
}

impl Default for CellGroupProps {
    fn default() -> Self {
        Self {
            rows: 10,
            columns: 10,
            mines: 10,
        }
    }
}

#[function_component]
pub fn CellGroup(props: &CellGroupProps) -> Html {
    // generate coords for a row
    let row_index = (1..=props.rows).collect::<Vec<u32>>();
    let column_index = (1..=props.columns).collect::<Vec<u32>>();
    // let cell_indices = (1..=10).collect::<Vec<_>>();
    html! {
        <div class={"ms-grid"}>
            <div class={"ms-cell-row"}>
        {
            row_index.into_iter().map(|_i| {
                html!{
                    <Cell />
                }
            }).collect::<Html>()
        }
            </div>
        </div>
    }
}
