use uuid::Uuid;
use yew::prelude::*;

use ::log;

use super::icons::Flag;

#[derive(Debug, Properties, PartialEq, Clone)]
pub struct CellProps {
    #[prop_or((0,0))]
    pub coord: (u32, u32),
    #[prop_or(false)]
    pub is_mined: bool,
    #[prop_or(1)]
    pub adjacent_mines: u32,
}

#[derive(PartialEq)]
pub enum CellRenderState {
    Flagged,
    Uncovered,
    Covered,
}

#[function_component]
pub fn Cell(props: &CellProps) -> Html {
    // TODO: refactor into reducer
    let is_flagged = use_state_eq(|| false);
    let is_hovered = use_state_eq(|| false);
    let is_covered = use_state_eq(|| true);
    let cell_render_state = use_state_eq(|| CellRenderState::Covered);

    let display_adjacency_degree = use_state_eq(|| 0);

    let class = classes!(
        "ms-cell",
        format!("adjacency-degree--{}", display_adjacency_degree.to_string())
    );
    let key = Uuid::new_v4().simple().to_string();

    let onmouseenter = {
        let is_hovered = is_hovered.clone();
        // Note: The callback's closure's full signature is | e:web_sys::MouseEvent | -> Self
        // but we don't need the event obj here.
        Callback::from(move |_| is_hovered.set(true))
    };

    let onmouseleave = {
        let is_hovered = is_hovered.clone();
        Callback::from(move |_| is_hovered.set(false))
    };

    let onclick = {
        let is_covered = is_covered.clone();
        let is_flagged = is_flagged.clone();
        let cell_render_state = cell_render_state.clone();

        let display_adjacency_degree = display_adjacency_degree.clone();
        let actual_adjacency_degree = props.adjacent_mines.clone();
        Callback::from(move |_| {
            if !*is_covered {
                // the cell is UN-COVERED
                is_covered.set(false); //do nothing
                cell_render_state.set(CellRenderState::Uncovered); //do nothing
            } else if *is_flagged {
                // the cell is FLAGGED
                is_flagged.set(false); // un-flag cell
                cell_render_state.set(CellRenderState::Covered); // set as COVERED
            } else {
                is_covered.set(false); //uncover the cell
                cell_render_state.set(CellRenderState::Uncovered); //set as UNCOVERED
                display_adjacency_degree.set(actual_adjacency_degree);
            }
        })
    };

    let oncontextmenu = {
        let is_flagged = is_flagged.clone();
        let is_covered = is_covered.clone();
        let cell_render_state = cell_render_state.clone();
        Callback::from(move |e: web_sys::MouseEvent| {
            e.prevent_default();
            if !*is_covered {
                // the cell is UNCOVERED
                cell_render_state.set(CellRenderState::Uncovered); //do nothing
            } else if *is_flagged {
                // the cell is FLAGGED
                is_flagged.set(false); // un-flag the cell
                cell_render_state.set(CellRenderState::Covered); // set as COVERED
            } else {
                // the cell is COVERED
                is_flagged.set(true); // flag the cell
                cell_render_state.set(CellRenderState::Flagged); //set as FLAGGED
            }
        })
    };

    html! {
      <div
          {class}
          {key}
          data-is_flagged={is_flagged.to_string()}
          data-is_hovered={is_hovered.to_string()}
          data-is_covered={is_covered.to_string()}
          //data-adjacency_degree={display_adjacency_degree.to_string()}
          { onmouseenter }
          { onmouseleave }
          { onclick }
          { oncontextmenu }
      >
          {
              match *cell_render_state.clone() {
                  CellRenderState::Flagged => html! { <Flag /> },
                  CellRenderState::Uncovered => {
                      if props.adjacent_mines > 0 {
                          html! {props.adjacent_mines}
                      } else {
                          html! {"-"}
                      }
                  },
                  CellRenderState::Covered => html! {}
              }

          }
      </div>
    }
}
