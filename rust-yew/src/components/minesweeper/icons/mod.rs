use yew::prelude::*;

#[function_component]
pub fn Flag() -> Html {
    html! {
        <svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
        <g>
        <title>{"background"}</title>
        <rect fill="none" id="canvas_background" height="52" width="52" y="-1" x="-1"/>
        <g display="none" overflow="visible" y="0" x="0" height="100%" width="100%" id="canvasGrid">
        <rect fill="url(#gridpattern)" stroke-width="0" y="0" x="0" height="100%" width="100%"/>
        </g>
        </g>
        <g>
        <title>{"Flag"}</title>
        <path id="svg_6" d="m4.5105,5.20324l-0.03498,40.531c0.03498,0.02866 40.5944,-20.53101 40.5944,-20.53101c0,0 1.95804,0.41958 -40.55942,-19.99999z" stroke-opacity="null" stroke-width="1.5" stroke="#000" fill="#ff0000"/>
        </g>
        </svg>
    }
}

#[function_component]
pub fn Clock() -> Html {
    html! {
        <svg width="50" height="50" xmlns="http://www.w3.org/2000/svg">
        <g>
        <title>{"background"}</title>
        <rect fill="#fff" id="canvas_background" height="52" width="52" y="-1" x="-1"/>
        <g display="none" overflow="visible" y="0" x="0" height="100%" width="100%" id="canvasGrid">
        <rect fill="url(#gridpattern)" stroke-width="0" y="0" x="0" height="100%" width="100%"/>
        </g>
        </g>
        <g>
        <title>{"Time"}</title>
        <ellipse ry="21.95803" rx="21.95803" id="svg_1" cy="25.62281" cx="25.06993" stroke-width="1.5" stroke="#000" fill="#fff"/>
        <path id="svg_3" d="m24.65035,7.44101l-0.03498,17.87366l12.02797,12.02797" opacity="0.5" fill-opacity="null" stroke-opacity="null" stroke-width="1.5" stroke="#000" fill="#fff"/>
        </g>
        </svg>
    }
}
