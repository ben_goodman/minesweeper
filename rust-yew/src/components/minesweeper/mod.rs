pub mod cell;
pub mod grid;
pub mod icons;
pub mod toolbar;
pub mod util;

use gloo::console::{self, Timer};
use gloo_timers::callback::{Interval, Timeout};
use yew::{function_component, html, Component, Html, Properties};

// use crate::components::minesweeper::cell::Cell;

use self::util::*;
use self::{cell::*, grid::*, toolbar::*};

#[derive(PartialEq, Properties)]
pub struct MinesweeperProps;

pub struct MinesweeperGame {
    //move time to context?
    time: GameTime,
    messages: Vec<&'static str>,
    _standalone: (Interval, Interval),
    interval: Option<Interval>,
    timeout: Option<Timeout>,
}

pub enum Msg {
    // StartTimeout,
    // StartInterval,
    // Cancel,
    // Done,
    // Tick,
    UpdateTime,
}

impl Component for MinesweeperGame {
    type Message = Msg;
    type Properties = MinesweeperProps;

    fn create(ctx: &yew::Context<Self>) -> Self {
        let standalone_handle = Interval::new(1_000, || {
            console::debug!("Example of a standalone callback.")
        });

        let clock_handle = {
            let link = ctx.link().clone();
            Interval::new(1_000, move || link.send_message(Msg::UpdateTime))
        };

        Self {
            time: GameTime::default(),
            messages: Vec::new(),
            _standalone: (standalone_handle, clock_handle),
            interval: None,
            timeout: None,
        }
    }

    fn update(&mut self, ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::UpdateTime => {
                self.time.update();
                true
            }
        }
    }

    fn view(&self, _ctx: &yew::Context<Self>) -> Html {
        // let cells = vec![Cell { props: CellProps{coord:(0,0),is_mined:false,adjacent_mines:0,is_flagged:false} }];
        html! {
          <div id={"ms-game-container"} >
            <Toolbar game_time = { self.time } />
            <CellGroup rows={10} columns={10} mines={10}/>
          </ div>
        }
    }
}
