use yew::{function_component, html, Html, Properties};

use super::{
    icons::{Clock, Flag},
    util::GameTime,
};

#[derive(Properties, PartialEq)]
pub struct ToolbarProps {
    #[prop_or_default]
    pub game_time: GameTime,
    #[prop_or_default]
    pub flags_remaining: u32,
}

impl Default for ToolbarProps {
    fn default() -> Self {
        Self {
            game_time: GameTime::default(),
            flags_remaining: 0,
        }
    }
}

#[function_component]
pub fn Toolbar(props: &ToolbarProps) -> Html {
    html! {
      <div id="toolbar-wrapper">
        <div class="icon-wrapper">
          <div class="icon"><Flag/></div>
          <div class="icon-lbl" id="flags-remaining">{ props.flags_remaining }</div>
        </div>
        <div class="icon-wrapper">
          <div class="icon" id="clock-icon"><Clock /></div>
          <div class="icon-lbl" id="time-elapsed">{ props.game_time.seconds }</div>
        </div>
      </div>
    }
}
