#[derive(Clone, Copy, Debug, PartialEq)]
pub struct GameTime {
    pub seconds: u32, //base 60 arithmetic
}

impl Default for GameTime {
    fn default() -> Self {
        Self {
            seconds: 0,
            // pretty: String::from("--:--")
        }
    }
}

impl GameTime {
    pub fn update(&mut self) -> () {
        self.seconds += 1
    }

    pub fn pretty(&self) -> &str {
        todo!()
    }
}

#[derive(PartialEq)]
pub struct Coord {
    pub m: u32, //row
    pub n: u32, //column
}
