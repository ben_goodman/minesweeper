use yew::prelude::*;

#[function_component]
pub fn Test() -> Html {
    let counter = use_state(|| 0);
    let onclick = {
        let counter = counter.clone();
        move |_| {
            let value = *counter + 1;
            counter.set(value);
        }
    };

    html! {
        <div class="test">
            <button {onclick}>{ "+1" }</button>
            <p>{"Counter: "}{ *counter }</p>
        </div>
    }
}
