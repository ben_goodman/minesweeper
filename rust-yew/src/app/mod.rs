use yew::prelude::*;
use yew_router::prelude::*;

use crate::routes::*;

#[function_component(Main)]
fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
        </BrowserRouter>
    }
}

pub fn render() {
    yew::Renderer::<Main>::new().render();
}
