use yew::prelude::*;
use yew_router::prelude::*;

use crate::components::minesweeper::*;
use crate::components::test::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/test")]
    Test,
    #[at("/minesweeper")]
    Minesweeper,
    #[not_found]
    #[at("/404")]
    NotFound,
}

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <h1>{ "Home" }</h1> },
        Route::Test => html! { <Test /> },
        Route::Minesweeper => html! { <MinesweeperGame /> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}
