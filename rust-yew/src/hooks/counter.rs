use std::rc::Rc;
use yew::Reducible;

/// reducer's Action
pub enum CounterAction {
    Increment,
    Decrement,
    Double,
    Square,
}

/// reducer's State
pub struct CounterState {
    pub counter: i32,
}

impl Default for CounterState {
    fn default() -> Self {
        Self { counter: 0 }
    }
}

impl Reducible for CounterState {
    /// Reducer Action Type
    type Action = CounterAction;

    /// Reducer Function
    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        let next_ctr = match action {
            CounterAction::Increment => self.counter + 1,
            CounterAction::Double => self.counter * 2,
            CounterAction::Decrement => self.counter - 1,
            CounterAction::Square => self.counter.pow(2),
        };

        Self { counter: next_ctr }.into()
    }
}
