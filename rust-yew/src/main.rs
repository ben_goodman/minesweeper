mod app;
mod components;
mod hooks;
mod routes;

use crate::app::render;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    render();
}
