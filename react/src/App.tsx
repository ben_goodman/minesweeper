import React from 'react'
import { Grid } from './components/Grid'
import { createGlobalStyle } from 'styled-components'


const GlobalStyle = createGlobalStyle`
  body {
    color: white;
    background-color: #222222;
    text-transform: uppercase;
    font-size: x-large;
    font-family: "Ubuntu Mono", monospace;
    margin: 10%;
  }
`

export const App = () => {
    return (
        <>
            <GlobalStyle />
            <Grid size='m'/>
        </>
    )
}
