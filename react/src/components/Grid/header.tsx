import React from 'react'
import { GameState } from '.'

export interface GridHeaderProps {
    flagCount: number
    gameStatus: GameState
    handleReset: () => void
}

export const GridHeader = (props: GridHeaderProps) => {
    return (
        <div>
            <h1>Minesweeper</h1>
            <ul>
                <li>Click to uncover a cell.</li>
                <li>Right-click to place a flag.</li>
                <li>Double-click an uncovered cell to automatically reveal all un-flagged neighbors.</li>
                <li>Reveal all un-mined cells to win.</li>
            </ul>
            <p>Flags left: {props.flagCount}</p>
            {props.gameStatus.hasWon && <span><p>You win!</p> <button onClick={() => props.handleReset()}>Play Again</button></span>}
            {props.gameStatus.hasLost &&  <span><p>You loose</p> <button onClick={() => props.handleReset()}>Play Again</button></span> }
        </div>
    )
}