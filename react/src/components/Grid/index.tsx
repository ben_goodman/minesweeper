import React from 'react'
import { Cell } from '../Cell'
import { CellRow, GridContainer } from './styles'
import {
    initializeGrid,
    uncoverCell,
    uncoverNeighbors,
    flagCell,
    checkLoose,
    checkWin
} from './util'
import { GridHeader } from './header'

export type GridSize = 'sm'|'m'|'l'|'xl'

const gridSpecs = {
    sm: {
        rows: 10,
        columns: 10,
        mines: 10
    },
    m: {
        rows: 30,
        columns: 30,
        mines: 100
    },
    l: {
        rows: 50,
        columns: 80,
        mines: 150
    },
    xl: {
        rows: 70,
        columns: 120,
        mines: 200
    }
}

export interface GridProps {
    size: GridSize
}

export interface GameState {
    hasWon: boolean
    hasLost: boolean
}

export const Grid = (props: GridProps) => {

    const {mines, rows, columns} = gridSpecs[props.size]

    const [flagCount, setFlagCount] = React.useState(mines)
    const [gameState, setGameState] = React.useState<GameState>({ hasWon: false, hasLost: false })
    const [now, setNow] = React.useState(0)
    const [grid, setGrid] = React.useState(initializeGrid(rows, columns, mines))

    // re-render the grid when a cell is uncovered
    const handleUncover = React.useCallback((coords: [number, number]) => {
        setGrid(oldGrid => uncoverCell(oldGrid, coords))
        setNow(_ => _ + 1)
    }, [grid])

    const resetGame = React.useCallback(() => {
        setGrid(initializeGrid(rows, columns, mines))
        setGameState({ hasWon: false, hasLost: false })
        setFlagCount(mines)
    }, [gameState])

    const handleFlag = React.useCallback((coords: [number, number]) => {
        if (flagCount === 0) {
            return
        }
        setGrid(oldGrid => flagCell(oldGrid, coords))
        setFlagCount(oldCount => oldCount - 1)
        setNow(_ => _ + 1)
    }, [flagCount, grid])

    const handleDoubleClick = React.useCallback((coords: [number, number]) => {
        setGrid(oldGrid => uncoverNeighbors(oldGrid, coords))
        setNow(_ => _ + 1)
    }, [grid])

    // check game state on render
    React.useEffect(() => {
        if (checkLoose(grid) && !gameState.hasLost) {
            setGameState( state => ({ ...state, hasLost: true }))
        }

        if (checkWin(grid)) {
            setGameState( state => ({ ...state, hasWon: true }))
        }
    }, [now])

    return (
        <>
            <GridHeader
                flagCount={flagCount}
                gameStatus={gameState}
                handleReset={resetGame}
            />
            <GridContainer>
                {
                    grid.map((row, rowIndex) => (
                        <CellRow key={rowIndex}>
                            {
                                row.map((cell, columnIndex) => (
                                    <Cell
                                        key={columnIndex}
                                        renderState={cell.renderState}
                                        neighbors={cell.neighbors}
                                        isMined={cell.isMined}
                                        onUncover={() => handleUncover([rowIndex, columnIndex])}
                                        onFlag={() => handleFlag([rowIndex, columnIndex])}
                                        onDoubleClick={() => handleDoubleClick([rowIndex, columnIndex])}
                                    />
                                ))
                            }
                        </CellRow>
                    ))
                }
            </GridContainer>
        </>
    )
}