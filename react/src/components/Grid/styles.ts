import styled from "styled-components";

export const CellRow = styled.div`
    display: flex;
`

export const GridContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 20px;
    margin-bottom: 20px;
`