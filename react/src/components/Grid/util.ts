import { CellState } from "../Cell"

export interface CellData {
    isMined: boolean
    neighbors: number
    renderState: CellState
}

// generate a grid of cells
// randomly assign mined cells
// compute the mined neighbor count for each un-mined cell
export const initializeGrid = (rows: number, columns: number, mines: number) => {
    const grid: CellData[][] = Array.from({ length: rows }, () => Array.from({ length: columns }, () => ({
        isMined: false,
        neighbors: 0,
        renderState: CellState.COVERED
    })))

    const mineIndices = new Set<number>()
    while (mineIndices.size < mines) {
        mineIndices.add(Math.floor(Math.random() * rows * columns))
    }

    for (const index of mineIndices) {
        const row = Math.floor(index / columns)
        const column = index % columns
        grid[row][column].isMined = true
    }

    for (let row = 0; row < rows; row++) {
        for (let column = 0; column < columns; column++) {
            if (grid[row][column].isMined) {
                continue
            }

            for (let i = -1; i <= 1; i++) {
                for (let j = -1; j <= 1; j++) {
                    if (i === 0 && j === 0) {
                        continue
                    }

                    const neighborRow = row + i
                    const neighborColumn = column + j
                    if (neighborRow < 0 || neighborRow >= rows || neighborColumn < 0 || neighborColumn >= columns) {
                        continue
                    }

                    if (grid[neighborRow][neighborColumn].isMined) {
                        grid[row][column].neighbors++
                    }
                }
            }
        }
    }

    return grid
}

// update the grid as the result of uncovering a cell
export const uncoverCell = (grid: CellData[][], cellCoord: [number, number]) => {

    const [row, column] = cellCoord

    // un-flag the cell if it is flagged
    if (grid[row][column].renderState === CellState.FLAGGED) {
        grid[row][column].renderState = CellState.COVERED
        return grid
    }

    grid[row][column].renderState = CellState.UNCOVERED

    if (grid[row][column].isMined) {
        console.log('GAME OVER')
        return grid
    }

    if (grid[row][column].neighbors > 0) {
        return grid
    }

    const rows = grid.length
    const columns = grid[0].length

    const queue: [number, number][] = [[row, column]]
    const visited = new Set<string>()
    visited.add(`${row},${column}`)

    while (queue.length > 0) {
        const [currentRow, currentColumn] = queue.shift()!

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (i === 0 && j === 0) {
                    continue
                }

                const neighborRow = currentRow + i
                const neighborColumn = currentColumn + j
                if (neighborRow < 0 || neighborRow >= rows || neighborColumn < 0 || neighborColumn >= columns) {
                    continue
                }

                if (visited.has(`${neighborRow},${neighborColumn}`)) {
                    continue
                }

                visited.add(`${neighborRow},${neighborColumn}`)

                if (grid[neighborRow][neighborColumn].isMined) {
                    continue
                }

                const currentState = grid[neighborRow][neighborColumn].renderState
                switch (currentState) {
                    case CellState.COVERED:
                        grid[neighborRow][neighborColumn].renderState = CellState.UNCOVERED
                        break
                    case CellState.UNCOVERED:
                        continue
                    case CellState.FLAGGED:
                        continue
                }

                if (grid[neighborRow][neighborColumn].neighbors === 0) {
                    queue.push([neighborRow, neighborColumn])
                }
            }
        }
    }

    return grid
}

// uncover the neighbors of a cell
export const uncoverNeighbors = (grid: CellData[][], cellCoord: [number, number]) => {
    const [row, column] = cellCoord
    const rows = grid.length
    const columns = grid[0].length

    for (let i = -1; i <= 1; i++) {
        for (let j = -1; j <= 1; j++) {
            if (i === 0 && j === 0) {
                continue
            }

            const neighborRow = row + i
            const neighborColumn = column + j
            if (neighborRow < 0 || neighborRow >= rows || neighborColumn < 0 || neighborColumn >= columns) {
                continue
            }

            // ignore flagged cells - only target COVERED
            if (grid[neighborRow][neighborColumn].renderState === CellState.COVERED) {
                uncoverCell(grid, [neighborRow, neighborColumn])
            }
        }
    }

    return grid
}
    

// flag or un-flag a cell
// clicking on a flagged cell un-flags it.
export const flagCell = (grid: CellData[][], cellCoord: [number, number]) => {
    const [row, column] = cellCoord
    const currentState = grid[row][column].renderState
    switch (currentState) {
        case CellState.COVERED:
            grid[row][column].renderState = CellState.FLAGGED
            break
        case CellState.FLAGGED:
            grid[row][column].renderState = CellState.COVERED
            break
        case CellState.UNCOVERED:
            break
    }
    return grid
}

// check if the game is won
// the game is won if all un-mined cells are uncovered
export const checkWin = (grid: CellData[][]) => {
    for (const row of grid) {
        for (const cell of row) {
            if (!cell.isMined && cell.renderState !== CellState.UNCOVERED) {
                return false
            }
        }
    }
    return true
}

// check if the game is lost
// the game is lost if a mined cell is uncovered
export const checkLoose = (grid: CellData[][]) => {
    for (const row of grid) {
        for (const cell of row) {
            if (cell.isMined && cell.renderState === CellState.UNCOVERED) {
                return true
            }
        }
    }
    return false
}