import styled from "styled-components"
import { CellState } from "."

export const CellStyle = styled.div<{
    $neighbors: number,
    $viewState?: CellState,
    $isMined?: boolean
}>`
    width: 30px;
    height: 30px;
    color: black;

    // give box-shadow to uncovered cells
    ${props => props.$viewState === CellState.COVERED && `
        box-shadow: inset -2px -2px 5px black, inset 3px 2px 5px white;
    `}

    // set cell borders
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 0
        ? `border: 1px solid white;`
        : `border: 1px solid black;`
    }

    // set hover style for covered cells
    ${props => props.$viewState === CellState.COVERED && `
        &:hover {
            background: cyan;
            cursor: pointer;
        }
    `}

    // set background color based on cell state
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 0 && `background: white;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 1 && `background: #FFCCCC;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 2 && `background: #FF9999;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 3 && `background: #FF8888;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 4 && `background: #FF7777;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 5 && `background: #FF6666;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 6 && `background: #FF5555;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 7 && `background: #FF4444;`}
    ${props => props.$viewState === CellState.UNCOVERED && props.$neighbors === 8 && `background: #FF3333;`}

    ${props => props.$viewState === CellState.COVERED && `background: grey;`}
    ${props => props.$viewState === CellState.FLAGGED && `background: green;`}
`