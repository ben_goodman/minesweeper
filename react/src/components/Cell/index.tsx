import React from 'react'
import { CellStyle } from './styles'

export enum CellState {
    COVERED = 'COVERED',
    UNCOVERED = 'UNCOVERED',
    FLAGGED = 'FLAGGED'
}

export interface CellProps {
    renderState: CellState
    neighbors: number
    isMined: boolean
    onUncover: () => void
    onFlag: () => void,
    onDoubleClick: () => void
}

const generateCellContent = (props: CellProps) => {
    if (props.renderState === CellState.COVERED) {
        return null
    }

    if (props.renderState === CellState.FLAGGED) {
        return '🚩'
    }

    if (props.isMined) {
        return '💣'
    }

    if (props.neighbors === 0) {
        return null
    }


    return props.neighbors
}

export const Cell = (props: CellProps) => {
    const timer = React.useRef<any>()

    const handleClick = (event: any) => {
        if (!event) {
            throw new Error('Cell click event is undefined')
        }
        console.debug(event)

        clearTimeout(timer.current);

        if (event.detail === 1) {
            timer.current = setTimeout(props.onUncover, 200)
        } else if (event.detail === 2) {
            props.onDoubleClick()
        }
    }

    const handleRightClick = (event: React.MouseEvent) => {
        event.preventDefault()
        props.onFlag()
    }

    return (
        <CellStyle
            $neighbors={props.neighbors}
            $viewState={props.renderState}
            $isMined={props.isMined}
            onClick={handleClick}
            onContextMenu={handleRightClick}
        >
            {generateCellContent(props)}
        </CellStyle>
    )

}